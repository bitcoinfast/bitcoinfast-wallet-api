class WalletsController < ApplicationController
  before_action :set_client, only: [:create]
  before_action :set_wallet, only: [:show, :update, :destroy]
  before_action :authenticate_wallet, except: [:create]

  # GET /wallets
  # GET /wallets.json
  # def index
  #   @wallets = Wallet.all
  # end

  # GET /wallets/1
  # GET /wallets/1.json
  def show
    @addresses = get_addresses
    @balances = get_balances
  end

  # POST /wallets
  # POST /wallets.json
  def create
    @wallet = Wallet.new(wallet_params)
    @wallet.uuid = SecureRandom.uuid
    @wallet.address = @client.getnewaddress

    if @wallet.save
      begin
        # create an account on the client
        @client.importprivkey(@client.dumpprivkey(@wallet.address), @wallet.uuid)
      rescue BitcoinRPC::Client::JSONRPCError => e
      end

      @jwt = Knock::AuthToken.new(payload: { sub: @wallet.id }).token

      render :show, status: :created, location: @wallet
    else
      render json: @wallet.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /wallets/1
  # PATCH/PUT /wallets/1.json
  def update
    @wallet = current_wallet

    # require current password to update
    #
    # if @wallet.authenticate(params[:wallet][:current_password])
    #   if @wallet.update_attributes(wallet_params)
    #     render :show, status: :ok, location: @wallet
    #   else
    #     render json: @wallet.errors, status: :unprocessable_entity
    #   end
    # else
    #   render json: { error: "Incorrect password" }, status: :unprocessable_entity
    # end

    # not require current password to update
    if @wallet.update_attributes(wallet_params)
      render :show, status: :ok, location: @wallet
    else
      render json: @wallet.errors, status: :unprocessable_entity
    end
  end

  # DELETE /wallets/1
  # DELETE /wallets/1.json
  def destroy
    @wallet.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wallet
      @wallet = current_wallet
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wallet_params
      params[:wallet].permit(:email, :password, :password_confirmation, :terms_of_service, :two_factor)
    end
end
