class TwoFactorController < ApplicationController
  before_action :authenticate_wallet
  before_action :set_wallet

  def create
    if(!@wallet.two_factor_enabled?)
      @wallet.set_google_secret
      render :show, status: :ok
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def destroy
    # require current password & 2fa code to deactivate
    #
    # if params[:wallet] && @wallet.authenticate(params[:wallet][:current_password]) && @wallet.google_authentic?(params[:wallet][:'2fa_code'])
    #   @wallet.clear_google_secret!
    #   render json: { message: "Two-factor authentication has been disabled." }, status: :ok
    # else
    #   render json: { error: "Incorrect password/ 2FA code" }, status: :unprocessable_entity
    # end

    # deactivate without requiring current password & 2fa code

    @wallet.clear_google_secret!
    render :show, status: :ok
  end

  private

    def set_wallet
      @wallet = current_wallet
      @wallet.skip_validation = true
    end
end
