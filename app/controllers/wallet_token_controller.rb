class WalletTokenController < Knock::AuthTokenController
  before_action :set_wallet

  def create
    if authenticated?
      if two_factor_authenticated?
        render json: Knock::AuthToken.new(payload: { sub: @wallet.id }), status: :created
      else
        if params[:auth][:'2fa_code']
          head :unprocessable_entity
        else
          head :unauthorized
        end
      end
    else
      head :not_found
    end
  end

  private

    def authenticated?
      @wallet.authenticate(params[:auth][:password]) ? true : false
    end

    def two_factor_authenticated?
      if(@wallet.two_factor_enabled?)
        @wallet.authenticate(params[:auth][:password]) && @wallet.google_authentic?(params[:auth][:'2fa_code']) ? true : false
      else
        @wallet.authenticate(params[:auth][:password]) ? true : false
      end
    end

    def set_wallet
      @wallet =  Wallet.find_by_email(params[:auth][:email])
    end
end
