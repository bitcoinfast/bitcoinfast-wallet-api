class ApplicationController < ActionController::API
  include Knock::Authenticable

  before_action :set_default_format
  # before_action :cors_set_access_control_headers

  private

    def wallet_has_address?(address)
      @addresses = get_addresses
      @addresses.include?(address)
    end

    def get_key_pairs
      @key_pairs = []

      get_addresses.each do |a|
        pair = {
          address: a,
          private_key: get_private_key(a),
        }

        @key_pairs.push(pair)
      end

      return @key_pairs
    end

    def logged_in?
      !current_wallet.nil?
    end

    def get_transactions(count = 10, from = 0)
      set_client
      @client.listtransactions(current_wallet.uuid, count, from)
    end

    def JSON_string_to_hash(string)
      JSON.parse(string.gsub('=>', ':'))
    end

    def is_address_valid?(address)
      set_client
      @client.validateaddress(address)["isvalid"]
    end

    def get_balance
      set_client
      balance = @client.getbalance(current_wallet.uuid)
    end

    def get_balances
    	set_client
      bcf_balance = @client.getbalance(current_wallet.uuid)
      # usd_rate = Float(JSON.parse(HTTP.get("https://api.coinmarketcap.com/v1/ticker/bitcoinfast/").to_s)[0]["price_usd"])
      # usd_balance = Float(bcf_balance) * usd_rate
      usd_rate = 0
      usd_balance = 0
      balances = [bcf_balance, usd_balance, usd_rate]
    end

    def set_default_format
      request.format = :json
    end

    def get_private_key(address)
      set_client
      private_key = @client.dumpprivkey(address)
    end

    def get_addresses
      set_client
      addresses = @client.getaddressesbyaccount(current_wallet.uuid)
    end

    def get_address
      address = current_wallet.address
    end

    def generate_new_address
      set_client
      @client.getnewaddress(current_wallet.uuid)
    end

    def set_client
      @client ||= BitcoinRPC::Client.new(ENV["RPC_URL"])
    end

    helper_method :get_balance, :get_balances, :get_transactions, :logged_in?, :wallet_has_address?
end
