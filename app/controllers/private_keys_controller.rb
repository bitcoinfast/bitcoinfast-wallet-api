class PrivateKeysController < ApplicationController
  before_action :authenticate_wallet
  before_action :set_client, only: [:create]
  # before_action :set_private_key, only: [:show, :update, :destroy]

  # GET /private_keys
  # GET /private_keys.json
  # def index
  #   @private_keys = PrivateKey.all
  # end

  # GET /private_keys/1
  # GET /private_keys/1.json
  # def show
  # end

  # POST /private_keys
  # POST /private_keys.json
  def create
    if defined?(params[:wallet][:private_key])
      if !params[:wallet][:private_key].empty?
        begin
          # create an account on the client
          @client.importprivkey(params[:wallet][:private_key], current_wallet.uuid)
        rescue BitcoinRPC::Client::JSONRPCError => e

          # stop executing & re-rendering the page if key is invalid
          if e.message == "{\"code\"=>-5, \"message\"=>\"Invalid private key\"}"
            render json: { message: 'Invalid private key' }, status: :unprocessable_entity
          else
            @addresses = get_addresses
            render :create, status: :ok
          end
        end
      else
        render json: { message: 'Invalid private key' }, status: :unprocessable_entity
      end
    else
      render json: '', status: :unprocessable_entity
    end
  end

  # PATCH/PUT /private_keys/1
  # PATCH/PUT /private_keys/1.json
  # def update
  #   if @private_key.update(private_key_params)
  #     render :show, status: :ok, location: @private_key
  #   else
  #     render json: @private_key.errors, status: :unprocessable_entity
  #   end
  # end

  # DELETE /private_keys/1
  # DELETE /private_keys/1.json
  # def destroy
  #   @private_key.destroy
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_private_key
    #   @private_key = PrivateKey.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def private_key_params
    #   params.fetch(:private_key, {})
    # end
end
