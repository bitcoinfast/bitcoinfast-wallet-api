class AddressesController < ApplicationController
  before_action :authenticate_wallet
  before_action :set_client, only: [:create]

  def show
    @address = params[:id]

    if(wallet_has_address?(@address))
      @private_key = get_private_key(@address)
      render :show, status: :ok
    else
      head :not_found
    end
  end

  def create
    # generate a new address & associating the address with current account
    @new_address = generate_new_address
    # save the address as current wallet address
    current_wallet.update_attribute(:address, @new_address)
    # return all addresses
    @addresses = get_addresses
  end
end
