class TransactionsController < ApplicationController
  before_action :set_client, only: [:create]
  before_action :authenticate_wallet
  # before_action :set_transaction, only: [:show, :update, :destroy]

  # GET /transactions
  # GET /transactions.json
  # def index
  #   @transactions = Transaction.all
  # end

  # GET /transactions/1
  # GET /transactions/1.json
  # def show
  # end

  # POST /transactions
  # POST /transactions.json
  def create
    fee = 0.01
    amount = params[:transaction][:amount].to_f
    address = params[:transaction][:address]

    if has_enough_balance?(amount, fee)
      if is_address_valid?(address)
        begin
          @client.settxfee(fee)
          @client.sendfrom(current_wallet.uuid, address, amount)
          render :create, status: :ok
        rescue BitcoinRPC::Client::JSONRPCError => e
          error_hash = JSON_string_to_hash(e.message)
          render json: error_hash, status: :unprocessable_entity
        end
      else
        render json: { message: "Invalid BCF address" }, status: :unprocessable_entity
      end
    else
      render json: { message: "Insufficient balance" }, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  # def update
  #   if @transaction.update(transaction_params)
  #     render :show, status: :ok, location: @transaction
  #   else
  #     render json: @transaction.errors, status: :unprocessable_entity
  #   end
  # end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  # def destroy
  #   @transaction.destroy
  # end

  private
    def has_enough_balance?(amount, fee)
      balance = get_balance
      spendable = balance - fee
      spendable = spendable.round(2)
      spendable >= amount ? true : false
    end

    # Use callbacks to share common setup or constraints between actions.
    # def set_transaction
    #   @transaction = Transaction.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def transaction_params
    #   params.fetch(:transaction, {})
    # end
end
