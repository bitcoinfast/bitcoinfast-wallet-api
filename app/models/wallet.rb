class Wallet < ApplicationRecord
  attr_accessor :skip_validation
  acts_as_google_authenticated :method => :wallet_email_with_label
  has_secure_password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_PASSWORD_REGEX = /\A.*(?=.{10,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*\z/
  validates :password, :password_confirmation, :email, :uuid, presence: true, on: :create
  validates :password, format: { with: VALID_PASSWORD_REGEX , message: "must be at least 10 characters, contains a number, an uppercase character and a special character"}, unless: :skip_validation
  validates :email, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }
  validates :terms_of_service, :acceptance => true, allow_nil: false, on: :create
  validates :email, uniqueness: true
  # non-updatable attributes
  attr_readonly :email

  def two_factor_enabled?
    !google_secret.nil?
  end

  private

    # knock authentication with uuid
    # def self.from_token_request request
    #   uuid = request.params["auth"] && request.params["auth"]["uuid"]
    #   self.find_by uuid: uuid
    # end

    def wallet_email_with_label
  	  "#{email} | BitcoinFast Wallet"
  	end
end
