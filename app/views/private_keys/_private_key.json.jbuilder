json.extract! private_key, :id, :created_at, :updated_at
json.url private_key_url(private_key, format: :json)
