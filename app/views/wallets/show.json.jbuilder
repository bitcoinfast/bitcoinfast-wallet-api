json.partial! "wallets/wallet", wallet: @wallet

# authentication required properties, only show these on wallets#show
if logged_in?
  json.backup_key @wallet.google_secret
  json.balance @balances[0]
  json.usd_balance @balances[1]
  json.usd_rate @balances[2]
  json.two_factor_enabled @wallet.two_factor_enabled?
  json.addresses @addresses
  json.transactions get_transactions
end

# show jwt after wallet creations
if @jwt
  json.jwt @jwt
end
