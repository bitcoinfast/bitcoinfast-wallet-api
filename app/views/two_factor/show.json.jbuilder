json.two_factor_enabled @wallet.two_factor_enabled?
json.google_secret @wallet.google_secret
json.qr_code @wallet.google_qr_uri
