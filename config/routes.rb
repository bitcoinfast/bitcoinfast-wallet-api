Rails.application.routes.draw do
	get '/' => 'pages#index'
  post 'wallet_token' => 'wallet_token#create'
  resources :addresses
  resources :private_keys
  resources :transactions
  resources :wallets
  resources :two_factor
end
