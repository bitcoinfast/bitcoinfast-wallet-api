class AddGoogleSecretToWallets < ActiveRecord::Migration[5.1]
  def change
    add_column :wallets, :google_secret, :string
  end
end
