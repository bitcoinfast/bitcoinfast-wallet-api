class CreateWallets < ActiveRecord::Migration[5.1]
  def change
    create_table :wallets do |t|
      t.string :email
      t.string :uuid
      t.string :password_digest
      t.string :address

      t.timestamps
    end
    add_index :wallets, :uuid
  end
end
