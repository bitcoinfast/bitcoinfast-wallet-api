# installation

* the BitcoinFast daemon:
- logs for the daemon is found in `~/.BitcoinFast/debug.log`

* commands:
- $ bundle install
- $ rake db:migrate

* variables
- wallets controller expects ENV["RPC_URL"]
- secret.yml expects ENV["SECRET_KEY_BASE"]
