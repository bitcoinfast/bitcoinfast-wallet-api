require 'test_helper'

class WalletsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @wallet = wallets(:one)
    @random_path = "utter-gibberish"
    @password = "TestPass@123"
    @not_taken_email = "nottaken@gmail.com"
  end

  test "should create wallet" do
    assert_difference('Wallet.count') do
      post wallets_url, params: { wallet: { email: @not_taken_email, password: @password, password_confirmation: @password, terms_of_service: 1 } }
    end

    assert_response 201
  end

  test "should show wallet" do
    get wallet_url(@random_path), headers: authenticated_header, as: :json

    assert_response :success
  end

  test "should not update wallet without auth token" do
    patch wallet_url(@random_path), params: { wallet: {  current_password: @password, password: 'NewPassword@123', password_confirmaion: 'NewPassword@123' } }, as: :json

    assert_response 401
  end

  test "should update wallet" do
    patch wallet_url(@random_path), params: { wallet: {  current_password: @password, password: 'NewPassword@123', password_confirmaion: 'NewPassword@123' } }, headers: authenticated_header, as: :json

    assert_response 200
  end

  test "uuid should not be updated" do
    new_uuid = "LOL"
    old_uuid = @wallet.uuid

    patch wallet_url(@random_path), params: { wallet: {  password: @password, password_confirmation: @password, uuid: new_uuid } }, headers: authenticated_header, as: :json

    json_response = JSON.parse(response.body)

    assert_equal old_uuid, json_response["uuid"]
  end

  test "email should not be updated" do
    old_email = @wallet.email
    new_email = "LOL@yahoo.com"

    patch wallet_url(@random_path), params: {  wallet: { current_password: @password, email: new_email } }, headers: authenticated_header, as: :json

    get wallet_url(@random_path), headers: authenticated_header, as: :json

    json_response = JSON.parse(response.body)

    assert_equal old_email, json_response["email"]
  end

  test "should not destroy wallet without auth token" do
    assert_difference('Wallet.count', 0) do
      delete wallet_url(@random_path), as: :json
    end

    # assert_response 204
    assert_response 401
  end

  test "should destroy wallet" do
    assert_difference('Wallet.count', -1) do
      delete wallet_url(@random_path), headers: authenticated_header, as: :json
    end

    assert_response 204
  end
end
