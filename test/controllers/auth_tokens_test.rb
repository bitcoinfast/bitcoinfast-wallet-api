require 'test_helper'

class AuthTokensTest < ActionDispatch::IntegrationTest
  setup do
    @wallet = wallets(:one)
  end

  test "should create jwt token" do
    post wallet_token_url, params: { auth: { email: @wallet.email, password: "TestPass@123" } }

    assert_response 201
  end

  test "should not create 2fa enabled jwt token without 2fa code" do
    # turn on 2fa
    post two_factor_index_url, headers: authenticated_header
    # try to log in
    post wallet_token_url, params: { auth: { email: @wallet.email, password: "TestPass@123" } }
    # fail
    assert_response 401
  end
end
