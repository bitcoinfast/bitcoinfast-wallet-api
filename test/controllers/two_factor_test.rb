require 'test_helper'

class TwoFactorTest < ActionDispatch::IntegrationTest
  setup do
    @wallet = wallets(:one)
  end

  test "should activate 2fa authentication" do
    post two_factor_index_url, headers: authenticated_header

    assert_response 200
  end

  test "should not activate 2fa auth if already activated" do
    post two_factor_index_url, headers: authenticated_header
    post two_factor_index_url, headers: authenticated_header

    assert_response 422
  end

  # test "should not deactivate 2fa without password" do
  #   delete two_factor_url(@wallet), headers: authenticated_header
  #
  #   assert_response 422
  # end
end
